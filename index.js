const express = require('express');
const app = require('./app');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();

const serverPort = process.env.PORT || 4000;

app.use(cors({origin: '*'}));
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());

const getResponse = (metodo, peticion) => {
    return {
        metodo: metodo,
        path: peticion.path,
        body: peticion.body,
        headers: peticion.headers,
        date: new Date()
    };
}

app.post('/webhook', (req, res, next) => {
    let response = getResponse('POST', req);
    console.log(response);
    res.status(200).send(response);
});

app.get('/webhook', (req, res, next) => {
    let response = getResponse('GET', req);
    console.log(response);
    res.status(200).send(response);
});

app.listen(serverPort, () => {
    console.log('Server corriendo en puerto: ' + serverPort);
})